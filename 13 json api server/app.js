var http = require('http'),
	url = require('url');

http.createServer(function (req, res) {

	var urlParsed = url.parse(req.url, true);
	var pathname = urlParsed.pathname;

	res.writeHead(200, {'Content-Type': 'application/json'});

	var date = new Date(urlParsed.query.iso);

	if (pathname === '/api/parsetime') {
		res.end(JSON.stringify({
			hour: date.getHours(),
			minute: date.getMinutes(),
			second: date.getSeconds()
		}));
	} else if (pathname === '/api/unixtime') {
		res.end(JSON.stringify({
			unixtime: date.getTime()
		}));
	}

}).listen(process.argv[2]);