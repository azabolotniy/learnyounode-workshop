var fs = require('fs'),
	path = require('path');

var folder = process.argv[2],
	filter = process.argv[3];

fs.readdir(folder, function (err, files) {
	for (var i=0; i<files.length; i++) {
		if ( path.extname(files[i]).replace('.', '') === filter ) {
			console.log(files[i])
		}
	}
});