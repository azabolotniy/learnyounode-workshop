
var fs = require('fs'),
	path = require('path');

module.exports = function (folder, filter, callback) {

	fs.readdir(folder, function (err, list) {
		if (err) {
			return callback(err);
		}
		
		var filteredList = [];

		list.forEach(function (file) {
			if ( path.extname(file).replace('.', '') === filter ) {
				filteredList.push(file);
			}
		})

		callback(null, filteredList);
	});
}