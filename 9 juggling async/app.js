var http = require('http'), 
	fs = require('fs');


var urls = [ process.argv[2], process.argv[3], process.argv[4] ];

function fetch(urls) {

	var fullResponse = '';

	if (urls.length === 0) {
		return;
	}	

	http.get(urls[0], function (response) {
		
		response.on('data', function (data) {
			fullResponse += data.toString();
		})

		response.on('end', function () {
			urls.shift();
			console.log(fullResponse);
			fetch(urls);
		})
	})
}

fetch(urls);

